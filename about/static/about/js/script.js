$(document).ready(function(){
	var allPanels=$('.panel').hide();
	var active;
	var clicked = false;

	$('.overlay').delay(2000);
	$('.overlay').slideUp(500);

	$( function() {
	    $( "#accordion" ).accordion({
	    	heightStyle: "content"
	    });
	    allPanels.slideUp();
	    if(this === active){
	      $(this).next().slideUp();
	      $(this).removeClass('active');
	      active = null;
	      return false;
	    }
	    $(active).removeClass('active');
	    $(this).addClass('active');
	    $(this).next().slideDown();
	    active = this;
	    return false;
  	});

	$('#theme').on('click', function() {
		if(clicked){
			$('body').css('background-color', '#F9E79F');
			$('#theme').css('background-color', '#FFF205');
			$('button').css('color', 'black');
			$('#theme > h4').text('Neon Mode');
			clicked = false;
		} else {
			$('body').css('background-color', '#FFF205');
			$('#theme').css('background-color', '#F9E79F');
			$('#theme > h4').text('Pastel Mode');
			clicked = true;
		};
		return false;
	});
});



function bookSearch() {
  var search = document.getElementById('search').value
  document.getElementById('results').innerHTML = ""
  console.log(search)

  $.ajax({
    url : 'https://www.googleapis.com/books/v1/volumes?q=' + search,
    dataType : "json",

    success:function(data) {
      for(i=0; i<data.items.length; i++){
        results.innerHTML += "<tr><td>"+"<img src=" + data.items[i].volumeInfo.imageLinks.thumbnail + 
        "height=200px width=200px/></td>"+"<td>" + data.items[i].volumeInfo.title + "</td><td>" + 
        data.items[i].volumeInfo.authors + "</td></tr>"
      }
    },

    type: 'GET'
  })
}

document.getElementById('button').addEventListener('click', bookSearch, false)