from django.test import TestCase, Client
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


class story7UnitTest(TestCase):
        def test_about_url_is_exist(self):
                response = Client().get('/about/')
                self.assertEqual(response.status_code, 200)

        def test_about_using_about_template(self):
                response = Client().get('/about/')
                self.assertTemplateUsed(response, 'about.html')
class story7FunctionalTest(TestCase):
        def setUp(self):
                chrome_options = Options()
                chrome_options.add_argument('--dns-prefetch-disable')
                chrome_options.add_argument('--no-sandbox')
                chrome_options.add_argument('--headless')
                chrome_options.add_argument('--disable-gpu')
                self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
                super(story7FunctionalTest,self).setUp()
        def tearDown(self):
                self.selenium.quit()
                super(story7FunctionalTest, self).tearDown()
        def test_change_theme_background(self):
                selenium = self.selenium
                selenium.get('http://127.0.0.1:8000/about/')
                css = selenium.find_element_by_tag_name('body')
                background = css.value_of_css_property('background-color')
                self.assertEqual(background, 'rgba(249, 231, 159, 1)')
                self.tearDown()



