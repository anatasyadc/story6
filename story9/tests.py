from django.test import TestCase, Client

class story9UnitTest(TestCase):
        def test_story9_url_is_exist(self):
                response = Client().get('/story9/signup/')
                self.assertEqual(response.status_code, 200)
        def test_story9_using_template(self):
                response = Client().get('/story9/signup/')
                self.assertTemplateUsed(response, 'regist.html')
