from django.urls import path,include
from .views import *

app_name = 'story9'

urlpatterns = [
    path('signup/', regist, name='regist'),
    path('', include('django.contrib.auth.urls'))

]