from django.test import TestCase, Client
from django.urls import reverse

# Create your tests here.
class Story10UnitTest(TestCase):

    def test_story10_url_exists(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_story10_using_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')
