from django.urls import path
from .views import *

app_name = 'story10'

urlpatterns = [
    path('', index, name='index'),
]