from django.test import TestCase, Client

class story8UnitTest(TestCase):
        def test_story8_url_is_exist(self):
                response = Client().get('/story8/')
                self.assertEqual(response.status_code, 200)

        def test_story8_using_about_template(self):
                response = Client().get('/story8/')
                self.assertTemplateUsed(response, 'listbuku.html')
