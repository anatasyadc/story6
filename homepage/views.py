from django.shortcuts import render
from django.shortcuts import redirect
from .forms import StatusForm
from .models import Status
from datetime import datetime, date

def index(request):
    data = Status.objects.all()
    form = StatusForm()

    content = {'title' : 'Status',
                'form' : form,
                'text' : 'Halo, apa kabar?',
                'data' : data}
    return render(request, 'landingpage.html', content)
def add_status(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('homepage:index')
