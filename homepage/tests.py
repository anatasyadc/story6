from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from django.http import HttpRequest
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .models import Status
from .forms import StatusForm
from datetime import date
import time

class story6UnitTest(TestCase):

        def test_story6_url_is_exist(self):
                response = Client().get('/story6/')
                self.assertEqual(response.status_code, 200)

        def test_story6_using_langdingpage_template(self):
                response = Client().get('/story6/')
                self.assertTemplateUsed(response, 'landingpage.html')

        def test_using_index_func(self):
                found = resolve('/story6/')
                self.assertEqual(found.func, index)
        def test_index(self):
                request = HttpRequest()
                response = index(request)
                html_response = response.content.decode('utf8')
                self.assertIn("Halo, apa kabar?", html_response)
        def test_model_create_new_status(self):
                new_status = Status.objects.create(dates=timezone.now(), status='pepew for life')
                counting_status_object = Status.objects.all().count()
                self.assertEqual(counting_status_object, 1)

        def test_forms_valid(self):
                form_data = {'status':'balblabla'}
                form = StatusForm(data=form_data)
                self.assertTrue(form.is_valid())
        def test_forms_not_valid(self):
                form_data = {'status':'blablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablabla'}
                form = StatusForm(data=form_data)
                self.assertFalse(form.is_valid())

class story6FunctionalTest(TestCase):
        def setUp(self):
                chrome_options = Options()
                chrome_options.add_argument('--dns-prefetch-disable')
                chrome_options.add_argument('--no-sandbox')
                chrome_options.add_argument('--headless')
                chrome_options.add_argument('--disable-gpu')
                self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
                super(story6FunctionalTest,self).setUp()
        def tearDown(self):
                self.selenium.quit()
                super(story6FunctionalTest, self).tearDown()
        def test_input_status(self):
                selenium = self.selenium
                selenium.get('http://127.0.0.1:8000/story6/')
                time.sleep(5);
                status = selenium.find_element_by_name('status')
                submit = selenium.find_element_by_id('submit')

                status_message = 'Coba coba'
                status.send_keys(status_message)
                submit.click()
                time.sleep(10)
        def test_element_h1_html(self):
                selenium = self.selenium
                selenium.get('http://127.0.0.1:8000/story6/')
                time.sleep(5);
                center = selenium.find_element_by_tag_name('h1')
                self.assertEqual(selenium.find_element_by_id('salam'), center)
        




                




        
